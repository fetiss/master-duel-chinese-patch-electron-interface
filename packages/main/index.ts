import { app, BrowserWindow, shell, ipcMain, session } from 'electron';
import { release } from 'os';
import { join } from 'path';
import childProcess from 'child_process'; // nodeJS 自带
import { initialize, enable } from '@electron/remote/main';

initialize();

const WINDOW_WIDTH = 1000;
const WINDOW_HEIGHT = 630;

// 高分屏，但是效果并不好，先删掉
// app.commandLine.appendSwitch('high-dpi-support', '1');
// app.commandLine.appendSwitch('force-device-scale-factor', '1');

// Disable GPU Acceleration for Windows 7
if (release().startsWith('6.1')) app.disableHardwareAcceleration();

// Set application name for Windows 10+ notifications
if (process.platform === 'win32') app.setAppUserModelId(app.getName());

if (!app.requestSingleInstanceLock()) {
  app.quit();
  process.exit(0);
}

// eslint-disable-next-line
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

let win: BrowserWindow | null = null;

async function createWindow(): Promise<void> {
  // 解决跨域问题 https://stackoverflow.com/questions/51254618/how-do-you-handle-cors-in-an-electron-app
  const filter = {
    urls: ['https://pub-94e9fb2ed7c543bb99e5deb80f18bd70.r2.dev/*'],
  };
  // 修改请求头
  session.defaultSession.webRequest.onBeforeSendHeaders(
    filter,
    (details, callback) => {
      // const url = new URL(details.url);
      // details.requestHeaders['Origin'] = `${url.protocol}//${url.host}`;
      callback({ requestHeaders: details.requestHeaders });
    }
  );
  // 修改响应头
  session.defaultSession.webRequest.onHeadersReceived(
    filter,
    (details, callback) => {
      details.responseHeaders!['Access-Control-Allow-Origin'] = ['*'];
      callback({ responseHeaders: details.responseHeaders });
    }
  );

  win = new BrowserWindow({
    title: 'No.86 汉化补丁',
    autoHideMenuBar: true,
    width: WINDOW_WIDTH,
    height: WINDOW_HEIGHT,
    minWidth: WINDOW_WIDTH,
    minHeight: WINDOW_HEIGHT,
    maxWidth: WINDOW_WIDTH,
    maxHeight: WINDOW_HEIGHT,
    frame: false,
    transparent: false,
    webPreferences: {
      preload: join(__dirname, '../preload/index.cjs'),
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  enable(win.webContents);

  if (app.isPackaged) {
    win.loadFile(join(__dirname, '../renderer/index.html'));
  } else {
    // 🚧 Use ['ENV_NAME'] avoid vite:define plugin
    const url = `http://${process.env['VITE_DEV_SERVER_HOST']}:${process.env['VITE_DEV_SERVER_PORT']}`;

    win.loadURL(url);
    // win.webContents.openDevTools()
  }

  // Test active push message to Renderer-process
  win.webContents.on('did-finish-load', () => {
    win?.webContents.send('main-process-message', new Date().toLocaleString());
  });

  // Make all links open with the browser, not with the application
  win.webContents.setWindowOpenHandler(({ url }) => {
    if (url.startsWith('https:')) shell.openExternal(url);
    return { action: 'deny' };
  });
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  win = null;
  if (process.platform !== 'darwin') app.quit();
});

app.on('second-instance', () => {
  if (win) {
    // Focus on the main window if the user tried to open another
    if (win.isMinimized()) win.restore();
    win.focus();
  }
});

app.on('activate', () => {
  const allWindows = BrowserWindow.getAllWindows();
  if (allWindows.length) {
    allWindows[0].focus();
  } else {
    createWindow();
  }
});

ipcMain.on('window-close', function () {
  (win as BrowserWindow).close();
});

ipcMain.on('window-minimize', function () {
  (win as BrowserWindow).minimize();
});

ipcMain.on('test', () => {
  console.log('test');
  viewProcessMessage('clash.exe', (res) => {
    console.log(res);
  });
});

const exec = childProcess.exec;

function viewProcessMessage(name: string, cb: (msg: string) => void) {
  // process 不用引入，nodeJS 自带
  // 带有命令行的list进程命令是：“cmd.exe /c wmic process list full”
  //  tasklist 是没有带命令行参数的。可以把这两个命令再cmd里面执行一下看一下效果
  // 注意：命令行获取的都带有换行符，获取之后需要更换换行符。可以执行配合这个使用 str.replace(/[\r\n]/g,""); 去除回车换行符
  let cmd = process.platform === 'win32' ? 'tasklist' : 'ps aux';
  exec(cmd, function (err, stdout, stderr) {
    if (err) {
      return console.error(err);
    }
    console.log(stdout);
    stdout.split('\n').filter((line) => {
      let processMessage = line.trim().split(/\s+/);
      let processName = processMessage[0]; //processMessage[0]进程名称 ， processMessage[1]进程id
      if (processName === name) {
        return cb(processMessage[1]);
      }
    });
  });
}
