import { PATCH, pathStorage } from '@utils';
import path from 'path';
import { defineStore } from 'pinia';

const dirPathSplit = path.normalize(__dirname).split(path.sep);

const clientPath =
  process.env.NODE_ENV === 'production'
    ? dirPathSplit.slice(0, dirPathSplit.indexOf('resources')).join(path.sep)
    : dirPathSplit
        .slice(0, dirPathSplit.indexOf('node_modules'))
        .join(path.sep);

// 如果是 prod -> E:/Code/MD Chinese Patch/Election 客户端/release/2.0.0/win-ia32-unpacked
// 如果是 dev -> E:/Code/MD Chinese Patch/Election 客户端

export const useStore = defineStore('main', {
  state: () => ({
    clientPath,
    gameRootPath: '',
    showDoc: false,
    showModalPatchSelect: false,
    patchType: PATCH.full,
    needUpdate: false,
  }),
  actions: {
    changePath(path: string) {
      this.gameRootPath = path;
      pathStorage.set(path);
    },
  },
});
