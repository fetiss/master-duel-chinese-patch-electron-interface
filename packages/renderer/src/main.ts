import { createPinia } from 'pinia';
import { createApp } from 'vue';

import App from './ConfigWrapper.vue';

const pinia = createPinia();
createApp(App)
  .use(pinia)
  .mount('#app')
  .$nextTick(window.removeLoading)
  .catch(console.error);
