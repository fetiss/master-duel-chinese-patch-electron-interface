import type EventEmitter from 'eventemitter3';

export {};

type PopupStatus =
  | 'success'
  | 'error'
  | 'info'
  | 'warning'
  | 'create'
  | 'destroyAll';
type Popup = Record<PopupStatus, (...args: any[]) => void>;

declare global {
  interface Window {
    removeLoading: () => void;
    msg: Popup & { loading: (...args: any[]) => void };
    notification: Popup;
    dialog: Popup;
    eventbus: EventEmitter;
  }
  const msg: Popup;
  const notification: Popup;
  const dialog: Popup;
  const eventbus: EventEmitter;
}
