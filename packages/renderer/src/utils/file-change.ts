import { useStore } from '@store';
import { checkPath } from '@utils';
import { nextTick } from 'vue';

export const fileChange = async (e: any): Promise<void> => {
  const file = e.target.files[0];
  e.target.value = '';
  if (file.name !== 'masterduel.exe') {
    dialog.error({
      title: '目录选择错误',
      content: '您没有选择 masterduel.exe 文件。”',
      positiveText: '确定',
      onPositiveClick: () => {
        changeGameRootPath('');
        msg.error('请重新选择');
      },
    });
    return;
  }
  changeGameRootPath((file.path as string).split('\\').slice(0, -1).join('\\'));
  await nextTick(() => {
    if (!checkPath(useStore().gameRootPath)) {
      return;
    }
    // 存到 cookie 里面
    msg.success('选择目录成功');
  });
};
function changeGameRootPath(s: string): void {
  useStore().changePath(s);
}
