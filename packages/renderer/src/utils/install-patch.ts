/* eslint-disable @typescript-eslint/restrict-template-expressions */

import { useStore } from '@store';
import {
  checkPath,
  copyDirSync,
  getVersion,
  PATCH,
  specialFiles,
} from '@utils';
import * as fs from 'fs';
import path from 'path';

// 补丁安装锁，防止多次点击
let isInstalling = false;
/**
 * 安装完全版/基础版补丁
 * @note 不能安装自适应补丁
 */
export async function installPatch(): Promise<void> {
  const { gameRootPath, clientPath, patchType } = useStore();
  const patchPath: string = path.join(clientPath, 'patch/CHINESE');
  if (!checkPath(gameRootPath)) return;
  if (isInstalling) {
    msg.error('补丁正在安装');
    return;
  }
  console.log('✨开始安装');
  console.warn('✨游戏路径', gameRootPath);
  console.warn('✨汉化补丁路径:', clientPath);
  console.warn('✨当前补丁版本:', getVersion());
  console.warn('✨安装版本:', patchType);
  msg.success(`开始安装 ${patchType as string}`);

  isInstalling = true;

  try {
    // 将补丁的0000文件夹复制到游戏的LocalData文件夹下的每一个xxxxxxxx文件夹下
    fs.readdirSync(path.join(gameRootPath, 'LocalData')).forEach((dir) => {
      if (dir === '00000000') return;
      copyDirSync(
        path.join(patchPath, '0000'),
        path.join(gameRootPath, 'LocalData', dir, '0000'),
        {
          // 基础版只安装字体
          filesWhiteList:
            patchType === PATCH.basic ? specialFiles.FONT : undefined,
        }
      );
    });
    // 将补丁的data.unity3d复制到游戏的masterduel_Data文件夹下
    fs.copyFileSync(
      path.join(patchPath, 'data.unity3d'),
      path.join(gameRootPath, 'masterduel_Data/data.unity3d')
    );
    msg.success('安装成功');
    console.log('安装成功');
  } catch (error: any) {
    console.error('安装失败:', error);
    msg.error('安装失败，请参照「帮助与设置」');
  } finally {
    isInstalling = false;
  }
}
