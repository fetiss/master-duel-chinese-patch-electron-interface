import { sleep } from '@utils';
import { exec } from 'child_process';
import flatten from 'lodash/flatten';

const keyPaths = [
  'HKEY_CURRENT_USER\\Software\\Valve\\Steam\\ActiveProcess',
  'HKEY_LOCAL_MACHINE\\SOFTWARE\\Valve\\Steam\\ActiveProcess',
];

export async function helper(keyPath: string): Promise<string> {
  let res = '';
  try {
    exec(`REG QUERY ${keyPath}`, (_error, stdout) => {
      const tmp = flatten(
        stdout.split('\r\n').map((s) => s.split('    '))
      ).filter((s) => s.includes('steamclient.dll'));
      res = tmp[0];
    });
  } catch (error) {
    console.log('🚨从注册表探测 Steam 路径失败了', error);
  }
  await sleep(120); // 留出 120ns 来查找注册表
  if (res) console.log('🦄从注册表探测到 Steam 路径:', res);
  return res;
}

/**
 * 通过注册表探测 Steam 位置
 */
export async function getSteamRoot(): Promise<string> {
  // eslint-disable-next-line
  for (let i = 0; i < keyPaths.length; i++) {
    const keyPath = keyPaths[i];
    const tmp = await helper(keyPath);
    if (tmp) return tmp.split('\\').slice(0, -1).join('\\');
    break;
  }
  return '';
}
