import { useStore } from '@store';
import {
  copyDirSync,
  copyFileAsync,
  existsSync,
  genCommand,
  getFilesListSync,
  getModifiedTimeSync,
  makeDirSync,
  removeDirSync,
} from '@utils';
import { exec } from 'child_process';
import * as fs from 'fs';
import path from 'path';

async function adaptive(): Promise<void> {
  const { gameRootPath, clientPath } = useStore();
  // 清空复制出来的文件的目标文件夹
  const adaptivePath = path.join(clientPath, 'patch/ADAPTIVE');
  removeDirSync(path.join(adaptivePath, 'ASSETS'));
  makeDirSync(path.join(adaptivePath, 'ASSETS/ORIGIN_ASSETS'));
  makeDirSync(path.join(adaptivePath, 'ASSETS/MODIFIED_ASSETS'));
  copyDirSync(
    path.join(adaptivePath, 'TOOLS/font_assets'),
    path.join(adaptivePath, 'ASSETS/MODIFIED_ASSETS/0000')
  );
  const adaptiveToolsPath = path.join(adaptivePath, 'TOOLS');
  const assetsList: string[] = JSON.parse(
    fs.readFileSync(path.join(adaptiveToolsPath, 'assets.json'), 'utf-8')
  );

  // xxxxxxxx 文件夹（访问时间最晚的那个）
  const x8Dir = getFilesListSync(path.join(gameRootPath, 'LocalData'))
    .filter((dir) => dir !== '0000000')
    .map((x8) => path.join(gameRootPath, 'LocalData', x8))
    .sort((a, b) => getModifiedTimeSync(b) - getModifiedTimeSync(a))[0];

  const getAssetPath = (assetName: string): string =>
    path.join(x8Dir, '0000', assetName.slice(0, 2), assetName);

  assetsList.forEach((assetName) => {
    // 探测是否存在
    const assetPath = getAssetPath(assetName);
    if (existsSync(assetPath)) {
      copyFileAsync(
        assetPath,
        path.join(adaptivePath, 'ASSETS', 'ORIGIN_ASSETS', assetName)
      );
    }
  });

  const command = genCommand(adaptiveToolsPath, clientPath);
  console.log(command);

  const res = await execAsync(command);
  console.log(res);

  if (res.includes('success')) {
    copyDirSync(
      path.join(clientPath, 'patch/ADAPTIVE/TOOLS/font_assets'),
      path.join(x8Dir, '0000')
    );
  }
}

const execAsync = async (command: string): Promise<string> =>
  await new Promise<string>((resolve, reject) => {
    exec(command, (err, stdout) => {
      if (err) reject(err);
      resolve(stdout);
    });
  });

let adaptiveLock = false;

export const installAdaptivePatch = async (): Promise<void> => {
  if (adaptiveLock) {
    msg.info('正在自适应中');
    return;
  }
  adaptiveLock = true;
  try {
    msg.info('自适应开始');
    await adaptive();
    msg.success('自适应成功');
  } catch (error) {
    msg.error('因未知原因失败');
  } finally {
    adaptiveLock = false;
  }
};
