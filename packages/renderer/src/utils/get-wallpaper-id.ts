type Ids = Array<{
  id: string;
  weight: number;
  arthor: string;
}>;

/**
 * 获取 ID，本质上是获取墙纸
 */
export function getId(): string {
  const totalWeight = idsWithWeight.reduce(
    (prev, { weight }) => prev + weight,
    0
  );
  idsWithWeight.forEach(
    ({ weight }, i) => (idsWithWeight[i].weight = weight / totalWeight)
  );
  let r = Math.random();
  for (const { id, weight } of idsWithWeight) {
    if (r < weight) return id;
    r -= weight;
  }
  return idsWithWeight.at(-1)!.id;
}

/**
 * ID 带权重
 */
const idsWithWeight: Ids = [
  {
    id: 'dragon',
    weight: 96,
    arthor: '@太斗',
  },
  {
    id: 'dark-magician-girl',
    weight: 4,
    arthor: '',
  },
];
