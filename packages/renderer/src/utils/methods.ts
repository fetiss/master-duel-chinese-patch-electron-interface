import { ipcRenderer, shell } from 'electron';

// 右上角的关闭 最小化 更多按钮
export function closeWindow(): void {
  ipcRenderer.send('window-close');
}
export function minimizeWindow(): void {
  ipcRenderer.send('window-minimize');
}

// 在浏览器打开补丁更新页面
export async function openInBrowser(href: string): Promise<void> {
  await shell.openExternal(href);
}

/**
 * 检查是否需要更新
 * @param localVersion 本地版本号
 * @param remoteVersion 远程版本号
 * @returns { update: boolean; isLargeUpdate: boolean } 是否需要更新，是否是大版本更新
 */
export function shouldUpdate(
  localVersion: string,
  remoteVersion: string
): { update: boolean; isLargeUpdate: boolean } {
  const localVersionArray = localVersion.split('.');
  const remoteVersionArray = remoteVersion.split('.');
  let update = false;
  let isLargeUpdate = false;

  for (let i = 0; i < localVersionArray.length; i++) {
    const local = parseInt(localVersionArray[i]);
    const remote = parseInt(remoteVersionArray[i]);
    if (remote > local) {
      update = true;
      if (i === 0) {
        isLargeUpdate = true;
      } else if (i === 1 && local !== 0) {
        isLargeUpdate = true;
      }
      break;
    } else if (remote < local) {
      break;
    }
  }

  return { update, isLargeUpdate };
}
