const gameRootPath = 'gameRootPath';
/**
 * 存取游戏路径
 * 现在暂时存到本地 JSON
 * 以后会改写的
 */
export const pathStorage = {
  set: (path: string) => {
    localStorage.setItem(gameRootPath, path);
  },
  get: (): string => {
    try {
      const res = localStorage.getItem(gameRootPath);
      if (res) console.log('✌️自动读取到游戏路径:', res);
      return res ?? '';
    } catch (error) {
      console.warn('🐔尝试读取本地储存的路径失败了');
      return '';
    }
  },
};
