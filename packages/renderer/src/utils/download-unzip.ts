import * as fs from 'fs';
import JSZip from 'jszip';
import { genUnit8ArraysMerger } from 'merge-unit8arrays';
import path from 'path';

export async function downloadArrayBuffer(
  url: string,
  updateInfoCb: (info: { totalSize: number; receivedSize: number }) => void
): Promise<ArrayBuffer> {
  const response = await fetch(url);
  const totalSize = parseInt(response.headers.get('content-length')!, 10);
  let receivedSize = 0;
  updateInfoCb({
    totalSize,
    receivedSize,
  });
  const Unit8ArraysMerger = genUnit8ArraysMerger();
  const receivedData = Unit8ArraysMerger;
  const readableStream = response.body!;
  const writableStream = new WritableStream<Uint8Array>({
    async write(chunk) {
      receivedSize += chunk.length;
      updateInfoCb({
        totalSize,
        receivedSize,
      });
      receivedData.push(chunk);
    },
  });
  await readableStream.pipeTo(writableStream);
  return receivedData.readBytes()!.buffer;
}

export async function unzipArrayBuffer(
  zipFileArrayBuffer: ArrayBuffer,
  tempPath: string
): Promise<void> {
  let zip = new JSZip();
  zip = await zip.loadAsync(zipFileArrayBuffer);
  for (const relativePath in zip.files) {
    const zipEntry = zip.files[relativePath];
    // 构造文件的绝对路径
    const filePath = path.resolve(tempPath, relativePath);
    // 如果是文件夹
    if (zipEntry.dir) {
      // 如果文件夹不存在，则创建
      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath, { recursive: true });
      }
    } else {
      // 如果是文件，则写入
      const fileData = await zipEntry.async('uint8array');
      fs.writeFileSync(filePath, fileData);
    }
  }
}
