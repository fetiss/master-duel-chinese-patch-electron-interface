import type { Ref } from 'vue';

/**
 * 选择游戏目录
 */
export const addUploadFile = (fileRef: Ref<HTMLElement>): void => {
  dialog.info({
    title: '提示',
    content,
    positiveText: '确定',
    onPositiveClick: () => {
      fileRef.value.dispatchEvent(new MouseEvent('click')); // fileRef是Vue3的写法
    },
  });
};

const content =
  '请选择游戏目录下的 masterduel.exe 文件。\n“游戏目录”指游戏王大师决斗的本地安装目录。\n不明白的话，可以百度一下steam查看游戏本地文件。';
