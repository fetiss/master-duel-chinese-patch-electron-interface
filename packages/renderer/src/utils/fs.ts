import * as fs from 'fs';
import path from 'path';

const pathJoin =
  <T>(f: (s: string) => T) =>
  (s: string) => {
    const _s = path.join(s);
    return f(_s);
  };

export const makeDirSync = pathJoin((dir: string) =>
  fs.mkdirSync(dir, { recursive: true })
);

export const removeDirSync = pathJoin((dir: string) => {
  if (existsSync(dir)) {
    const files = fs.readdirSync(dir);
    for (let i = 0; i < files.length; i++) {
      const newPath = path.join(dir, files[i]);
      const stat = fs.statSync(newPath);
      if (stat.isDirectory()) {
        // 如果是文件夹就递归下去
        removeDirSync(newPath);
      } else {
        // 删除文件
        fs.unlinkSync(newPath);
      }
    }
    fs.rmdirSync(dir); // 如果文件夹是空的，就将自己删除掉
  }
});

export async function copyFileAsync(
  src: string,
  tar: string
): Promise<unknown> {
  return await new Promise((resolve) => {
    fs.copyFile(path.join(src), path.join(tar), resolve);
  });
}

// 修改时间
export const getModifiedTimeSync = pathJoin((filename: string) => {
  return new Date(fs.statSync(filename).ctime).getTime();
});

export const getFilesListSync = pathJoin((dir: string) => {
  return fs.readdirSync(dir);
});

export const existsSync = pathJoin((file: string): boolean => {
  return fs.existsSync(file);
});

export const genCommand = (
  adaptiveToolsPath: string,
  clientPath: string
): string => {
  const _adaptiveToolsPath = path.normalize(adaptiveToolsPath);
  const _clientPath = path.normalize(clientPath);
  const originAssets = path.join(
    _clientPath,
    '/patch/ADAPTIVE/ASSETS/ORIGIN_ASSETS'
  );
  const json = path.join(_adaptiveToolsPath, '/json');
  const modifiedAssets = path.join(
    _clientPath,
    '/patch/ADAPTIVE/ASSETS/MODIFIED_ASSETS/0000'
  );

  // 如果是 IDS, 后面的参数分别是 asset 文件夹、json 文件夹、output 文件夹、资源表路径
  const command = [
    path.normalize('.\\patch\\ADAPTIVE\\TOOLS\\unitypy-box.exe'),
    'IDS',
    ...[originAssets, json, modifiedAssets].map((x) => `"${path.join(x)}"`),
  ].join(' ');
  return command;
};

/**
 * 同步复制文件夹
 * @function
 * @param {string} src - 源文件夹路径
 * @param {string} dest - 目标文件夹路径
 * @param {Object} [options={}] - 选项
 * @param {string[]} [options.filesWhiteList] - 文件白名单
 * @param {string[]} [options.filesBlackList] - 文件黑名单
 */

export function copyDirSync(
  src: string,
  dest: string,
  options: {
    filesWhiteList?: string[];
    filesBlackList?: string[];
  } = {}
): void {
  const files = fs.readdirSync(src);

  if (!fs.existsSync(dest)) {
    fs.mkdirSync(dest);
  }

  files.forEach((file) => {
    const current = path.join(src, file);
    const target = path.join(dest, file);
    if (fs.lstatSync(current).isDirectory()) {
      copyDirSync(current, target, options);
    } else {
      if (
        options.filesWhiteList?.length &&
        !options.filesWhiteList.includes(file)
      ) {
        return;
      }
      if (
        options.filesBlackList?.length &&
        options.filesBlackList.includes(file)
      ) {
        return;
      }
      fs.copyFileSync(current, target);
    }
  });
}
