export function urlJoin(...args: string[]): string {
  // Join all the arguments with '/'
  const url = args.join('/');

  // Remove any duplicate '/'
  const urlWithoutDuplicateSlashes = url.replace(/\/{2,}/g, '/');

  // Remove trailing '/'
  const finalUrl = urlWithoutDuplicateSlashes.replace(/\/$/, '');

  return finalUrl;
}
