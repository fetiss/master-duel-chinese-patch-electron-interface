import packageJson from '@package.json';
import { useStore } from '@store';
import * as fs from 'fs';
import path from 'path';

export function getVerFromJson(jsonPath: string): string {
  const config = fs.readFileSync(jsonPath, 'utf-8');
  return JSON.parse(config).version;
}

function getPatchVersion(): string {
  const { clientPath } = useStore();
  const v0000 = getVerFromJson(
    path.join(clientPath, '/patch/CHINESE/v.0000.json')
  ).split('.');
  const vDataUnity3d = getVerFromJson(
    path.join(clientPath, '/patch/CHINESE/v.data_unity3d.json')
  ).split('.');
  return `${vDataUnity3d[0]}.${vDataUnity3d[1]}.${v0000[2]}`;
}

export function getVersion(isClientVersion: boolean = false): string {
  const packageJsonVer = packageJson.version;
  if (isClientVersion) return packageJsonVer;
  let patchVersion: string;
  try {
    patchVersion = getPatchVersion();
  } catch (error) {
    console.log('getPatchVersion error', error);
    patchVersion = '0.0.0';
  }
  // compare version, return the higher one
  function compareVer(v1: string, v2: string): string {
    const v1Arr = v1.split('.');
    const v2Arr = v2.split('.');
    for (let i = 0; i < v1Arr.length; i++) {
      if (Number(v1Arr[i]) > Number(v2Arr[i])) {
        return v1;
      } else if (Number(v1Arr[i]) < Number(v2Arr[i])) {
        return v2;
      }
    }
    return v1;
  }
  return compareVer(packageJsonVer, patchVersion);
}
