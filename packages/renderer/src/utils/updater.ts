import { downloadArrayBuffer, unzipArrayBuffer, urlJoin } from '@utils';

const defaultUpdaterOptions = {
  downloadUrlBase: 'https://pub-94e9fb2ed7c543bb99e5deb80f18bd70.r2.dev/',
  extractPath: '~/Downloads/tmp',
};

enum Status {
  idle = 'idle',
  downloading = 'downloading',
  extracting = 'extracting',
  success = 'success',
  error = 'error',
}

type UpdateRecall = (info: {
  status: Status;
  totalSize: number;
  receivedSize: number;
}) => void;

class Updater {
  status = Status.idle;
  receivedSize = 0;
  totalSize = 0;
  updateRecall?: UpdateRecall;
  // singleton
  private static instance: Updater | undefined;
  constructor(updateRecall?: UpdateRecall) {
    this.updateRecall = updateRecall;
  }

  static getInstance(updateRecall?: UpdateRecall): Updater {
    if (Updater.instance == null) {
      Updater.instance = new Updater(updateRecall);
    }
    const info = {
      status: Status.idle,
      totalSize: 0,
      receivedSize: 0,
      downloadProgress: 0,
    };
    // 当数据发生变化时，调用updateRecall
    Object.keys(info).forEach((key) => {
      Object.defineProperty(Updater.instance, key, {
        get() {
          // @ts-expect-error: 没法调和 ts 的类型检查
          return info[key];
        },
        set(newValue) {
          // @ts-expect-error: 没法调和 ts 的类型检查
          info[key] = newValue;
          updateRecall?.(info);
        },
      });
    });
    return Updater.instance;
  }

  async downloadUpdate(
    ver: string,
    isfullUpdate: boolean,
    options: {
      downloadUrlBase?: string;
      extractPath?: string;
    } = defaultUpdaterOptions
  ): Promise<void> {
    // 如果正在下载或者正在解压，则不再下载
    if (this.status !== 'idle') {
      return;
    }
    this.setStatus(Status.downloading);
    // ver: 2.1.3
    const minorVer = ver.split('.').slice(0, 2).join('.'); // 2.1
    const patchVer = ver; // 2.1.3
    const downloadUrlBase =
      options.downloadUrlBase ?? defaultUpdaterOptions.downloadUrlBase;
    const extractPath =
      options.extractPath ?? defaultUpdaterOptions.extractPath;
    const minorUrl = urlJoin(downloadUrlBase, `v${minorVer}-data_unity3d.zip`);
    const patchUrl = urlJoin(downloadUrlBase, `v${patchVer}-0000.zip`);
    try {
      const downloadAndUnzip = async (urls: string[]): Promise<void> => {
        const infos = urls.map((_) => ({
          totalSize: 0,
          receivedSize: 0,
        }));
        const infoCbs = urls.map(
          (_, index) => (info: { totalSize: number; receivedSize: number }) => {
            infos[index] = info;
            this.totalSize = infos.reduce((a, b) => a + b.totalSize, 0);
            this.receivedSize = infos.reduce((a, b) => a + b.receivedSize, 0);
          }
        );
        const buffers = await Promise.all(
          urls.map((url, index) => downloadArrayBuffer(url, infoCbs[index]))
        );
        this.setStatus(Status.extracting);
        await Promise.all(
          buffers.map((buffer) => unzipArrayBuffer(buffer, extractPath))
        );
      };
      if (isfullUpdate) {
        await downloadAndUnzip([minorUrl, patchUrl]);
      } else {
        await downloadAndUnzip([patchUrl]);
      }
      this.setStatus(Status.success);
    } catch (error) {
      console.warn(error);
      this.setStatus(Status.error);
    }
  }

  cancelUpdate(): void {
    this.status = Status.idle;
    this.reset();
  }

  reset(): void {
    this.status = Status.idle;
    this.receivedSize = 0;
    this.totalSize = 0;
  }

  // 修改status
  setStatus(status: Status): void {
    this.status = status;
    eventbus.emit(`updater:${status}`);
  }
}

export { Updater, Status as UpdaterStatus };
