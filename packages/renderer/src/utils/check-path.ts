import * as fs from 'fs';
/**
 * 检测目录合法性
 */
export function checkPath(path: string): boolean {
  if (path.length === 0) {
    msg.error('请先选择游戏路径后再安装补丁');
    return false;
  }
  const i = path.lastIndexOf('\\');
  const folder = i > -1 ? path.substring(i + 1) : path;
  if (folder !== 'Yu-Gi-Oh!  Master Duel') {
    const errorMsg = `当前选择目录名称[${folder}]与游戏目录名称[Yu-Gi-Oh!  Master Duel]不符，请选择游戏目录下的masterduel.exe文件`;
    msg.error(errorMsg);
    return false;
  }
  if (!fs.existsSync(path + '\\LocalData')) {
    const errorMsg =
      '当前选择目录下未找到LocalData文件，可能游戏还未启动过，请在启动游戏并下载资源后再安装汉化补丁';
    msg.error(errorMsg);
    return false;
  }
  return true;
}
