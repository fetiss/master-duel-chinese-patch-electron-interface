export enum PATCH {
  full = '进阶版(默认)',
  basic = '基本版',
  adaptive = '自适应',
  // recover = '还原补丁',
}

interface optionType {
  label: string;
  key: string;
}

export const options: optionType[] = [
  {
    label: PATCH.full,
    key: PATCH.full,
  },
  {
    label: PATCH.basic,
    key: PATCH.basic,
  },
];

/**
 * 特殊文件：一般是各个版本都需要复制的文件
 */
export const specialFiles = {
  FONT: ['f61f6436', '3bd6d4e6', 'eb1eb6f7', 'f15c6c2a'],
};

type MoreOptionsType = Array<{
  label: string;
  key: string;
}>;

export const moreOptions: MoreOptionsType = [
  {
    label: '手动替换补丁',
    key: '手动替换补丁',
  },
];
