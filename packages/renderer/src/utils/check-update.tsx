import { shouldUpdate, openInBrowser, getVersion } from '@utils';
import { NButton, NSpace } from 'naive-ui';
import { useStore } from '@store';

interface UpdateInfo {
  version: string; // 2.0.1
  desc: string; // 描述
  updatedAt: string; // 2022-10-10T16:56:46.174Z
  download_links: Array<{
    // 下载链接
    url: string;
    name: string;
  }>;
  post: {
    // b站帖子
    url: string;
    title: string;
  };
}

const genDownloadList = (res: UpdateInfo) => (
  <div>
    <ul class="notify-ul">
      {res.download_links.map(({ url, name }) => (
        <li onClick={() => openInBrowser(url)}>{name}</li>
      ))}
    </ul>
  </div>
);

const genDownloadMeta = (data: UpdateInfo, isLargeUpdate: boolean) => (
  <div>
    <span style="display: flex">
      <span>补丁发布页：</span>
      <span class="post" onClick={() => openInBrowser(data.post.url)}>
        {data.post.title}
      </span>
    </span>
    {isLargeUpdate ? (
      <div>
        <strong>大型更新</strong>：请务必更新后再安装⚠️
      </div>
    ) : (
      <div>
        <strong>小型更新</strong>：不更新也没啥问题✨
      </div>
    )}

    <div>{`更新时间：${data.updatedAt.slice(0, 10)}`}</div>
  </div>
);

const backupData = {
  updatedAt: '2022-11-09T07:56:29.424Z',
  version: '2.1.0',
  desc: '本次更新为适配1109的MD大更新，对必要文字进行了补全翻译，其余翻译沿用上一版本，更完善的汉化将在2.1.1放出。',
  post: {
    url: 'https://www.bilibili.com/read/cv19640288',
    title: '游戏王大师决斗汉化补丁发布页',
  },
  download_links: [
    {
      url: 'https://pan.baidu.com/s/16IW4vYA5sfnHrlTtBjvUxA?pwd=4inf',
      name: '百度网盘',
    },
    {
      url: 'https://pan.quark.cn/s/fb7c0fc412d9',
      name: '夸克网盘',
    },
  ],
  objectId: '634190ab318a6c5d6f0154c7',
};

async function fetchLeanCloud(): Promise<UpdateInfo | undefined> {
  const url =
    'https://lean.lfcky.com/1.1/classes/update?order=-createdAt&limit=1';
  const resp = await fetch(url, {
    headers: new Headers({
      'Content-Type': 'application/json',
      'X-LC-Id': 'bco27x8UBwFWTeizzuDRwhnV-gzGzoHsz',
      'X-LC-Key': '7BplaEYy5JizibhpS3JYIwVY',
    }),
  });
  return resp.status === 200 ? (await resp.json()).results[0] : undefined;
}

async function fetchCloudFlare(): Promise<UpdateInfo | undefined> {
  const url = 'https://pub-94e9fb2ed7c543bb99e5deb80f18bd70.r2.dev/update.json';
  const resp = await fetch(url);
  return resp.status === 200 ? await resp.json() : undefined;
}

export async function checkUpdate() {
  msg.info('正在检查更新…');
  notification.destroyAll();
  let updateInfo: UpdateInfo = backupData;
  try {
    const tmp = (await fetchLeanCloud()) ?? (await fetchCloudFlare());
    msg.destroyAll();
    if (!tmp) throw new Error();
    updateInfo = tmp;
  } catch (error) {
    msg.error('获取更新信息失败');
    notification.error({
      title: `无法获取最新补丁信息`,
      meta: () => genDownloadMeta(backupData, false),
      content: '点击“补丁发布页”获取帮助',
      duration: 20000,
    });
    notification.error({
      title: '下载链接',
      content: () => genDownloadList(backupData),
      duration: 20000,
    });
    return;
  }
  const localVersion = getVersion();
  const { isLargeUpdate, update } = shouldUpdate(
    localVersion,
    updateInfo.version
  );
  if (update) {
    useStore().needUpdate = true;
    const updateInside = () => {
      eventbus.emit('quick-update', {
        version: updateInfo.version,
        isFullUpdate: isLargeUpdate,
      });
      notification.destroyAll();
    };
    const updateManual = () => {
      showDownloadList(updateInfo);
    };
    eventbus.on('manual-update', updateManual);
    notification.info({
      title: `补丁可更新至 v${updateInfo.version}`,
      meta: () => genDownloadMeta(updateInfo, isLargeUpdate),
      content: updateInfo.desc,
      duration: 20000,
    });
    notification.info({
      title: '下载更新',
      duration: 20000,
      content: () => (
        <NSpace>
          <NButton type="info" onClick={updateInside}>
            快速更新
          </NButton>
          <NButton secondary onClick={updateManual}>
            手动更新
          </NButton>
        </NSpace>
      ),
    });
  } else {
    notification.success({
      title: `补丁已更新至最新版本 v${localVersion}`,
      meta: `更新时间：${updateInfo.updatedAt.slice(0, 10)}`,
      duration: 5000,
    });
  }
}

function showDownloadList(data: UpdateInfo) {
  notification.info({
    title: '下载链接',
    content: () => genDownloadList(data),
    duration: 20000,
  });
}
