import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import { resolve as pathResolve } from 'path';
import { defineConfig, loadEnv } from 'vite';
import electron from 'vite-plugin-electron/renderer';
import resolve from 'vite-plugin-resolve';
import svgLoader from 'vite-svg-loader';
import tsconfigPaths from 'vite-tsconfig-paths';

import pkg from '../../package.json';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  loadEnv(mode, process.cwd());
  return {
    mode: process.env.NODE_ENV,
    root: __dirname,
    plugins: [
      tsconfigPaths(), // TODO: 无效，需要手动配置
      vue(),
      vueJsx(),
      electron(),
      resolve(
        /**
         * Here you can specify other modules
         * 🚧 You have to make sure that your module is in `dependencies` and not in the` devDependencies`,
         *    which will ensure that the electron-builder can package it correctly
         */
        {
          // If you use electron-store, this will work - ESM format code snippets
          // 'electron-store':
          //   'const Store = require("electron-store"); export default Store;',
        }
      ),
      svgLoader(),
    ],
    build: {
      outDir: '../../dist/renderer',
      emptyOutDir: true,
      sourcemap: true,
    },
    server: {
      host: pkg.env.VITE_DEV_SERVER_HOST,
      port: pkg.env.VITE_DEV_SERVER_PORT,
    },
    resolve: {
      alias: {
        '@store': pathResolve(__dirname, 'src/store'),
        '@utils': pathResolve(__dirname, 'src/utils'),
        '@package.json': pathResolve(__dirname, '../../package.json'),
      },
    },
  };
});
