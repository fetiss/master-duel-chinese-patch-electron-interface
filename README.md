# Yu-Gi-Oh! Master Duel 汉化补丁客户端

![UI](public/UI.png)

🥳 使用 `Electron` + `Vue` + `Vite` 构建.


## 快速开始

```sh
npm install
```

## 开发

```sh
npm run dev
```

## 构建

```sh
npm run build
```

## 温馨提示

使用前，请在`packages/renderer/.env.development`中配置开发路径，并且将汉化补丁的`patch`文件夹复制到根目录下。
